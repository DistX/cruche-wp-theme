
jQuery(document).ready(function ($) {

    ymaps.ready(function () {
        $('#address').keyup(function(){
            var val = $('#address').val();
            console.log(val);
            var myGeocoder = ymaps.geocode(val);


            myGeocoder.then(
                function (res) {
                    // alert('Координаты объекта :' + res.geoObjects.get(0).geometry.getCoordinates());
                    console.log([res.geoObjects.get(0).geometry.getCoordinates()]);
                    $('#ayp_map_coord').val(res.geoObjects.get(0).geometry.getCoordinates());
                    mapInit(res.geoObjects.get(0).geometry.getCoordinates());

                },
                function (err) {
                    alert('Ошибка инициализации карты');
                }
            );
            // mapInit(myGeocoder);
        });
        var myMap = new ymaps.Map('ayp_map',
            {
                center: $('#ayp_map_coord').val().split(','),
                zoom: 16,
                controls: []},
            {searchControlProvider: 'yandex#search'});
            var myPlacemark = new ymaps.Placemark($('#ayp_map_coord').val().split(','),
                {hintContent: '',balloonContent: ""}, { iconLayout: 'default#image',});

            myMap.behaviors.disable('scrollZoom');
            myMap.geoObjects.add(myPlacemark);

        function mapInit(t){

                myMap.setCenter(t);
                myMap.setGeoPoint(t);
        }
    });

});