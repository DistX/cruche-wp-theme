<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "ayp_redux";
global $ayp_redux;
// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters( 'ayp_redux/opt_name', $opt_name );

/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
    Redux_Functions::initWpFilesystem();

    global $wp_filesystem;

    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
}

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns      = array();

if ( is_dir( $sample_patterns_path ) ) {

    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
        $sample_patterns = array();

        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                $name              = explode( '.', $sample_patterns_file );
                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name'             => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'         => $theme->get( 'Name' ),
    // Name that appears at the top of your panel
    'display_version'      => $theme->get( 'Version' ),
    // Version that appears at the top of your panel
    'menu_type'            => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'       => true,
    // Show the sections below the admin menu item or not
    'menu_title'           => __( 'Настройки темы', 'redux-framework-demo' ),
    'page_title'           => __( 'Настройки темы', 'redux-framework-demo' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key'       => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography'     => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar'            => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon'       => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority'   => 50,
    // Choose an priority for the admin bar menu
    'global_variable'      => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode'             => false,
    // Show the time the page took to load, etc
    'update_notice'        => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer'           => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority'        => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent'          => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions'     => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon'            => '',
    // Specify a custom URL to an icon
    'last_tab'             => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon'            => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug'            => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults'        => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show'         => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark'         => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export'   => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time'       => 60 * MINUTE_IN_SECONDS,
    'output'               => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag'           => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database'             => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn'              => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints'                => array(
        'icon'          => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color'    => 'lightgray',
        'icon_size'     => 'normal',
        'tip_style'     => array(
            'color'   => 'red',
            'shadow'  => true,
            'rounded' => false,
            'style'   => '',
        ),
        'tip_position'  => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect'    => array(
            'show' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'mouseover',
            ),
            'hide' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'click mouseleave',
            ),
        ),
    )
);

// Panel Intro text -> before the form
if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
    if ( ! empty( $args['global_variable'] ) ) {
        $v = $args['global_variable'];
    } else {
        $v = str_replace( '-', '_', $args['opt_name'] );
    }
    $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
} else {
    $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
}

// Add content after the form.
$args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */


/*
 * ---> START HELP TABS
 */

$tabs = array(
    array(
        'id'      => 'redux-help-tab-1',
        'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
    ),
    array(
        'id'      => 'redux-help-tab-2',
        'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
    )
);
Redux::setHelpTab( $opt_name, $tabs );

// Set the help sidebar
$content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
Redux::setHelpSidebar( $opt_name, $content );


/*
 * <--- END HELP TABS
 */


/*
 *
 * ---> START SECTIONS
 *
 */

/*

    As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Общее', 'redux-framework-demo' ),
    'id'    => 'general',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture'
) );
Redux::setSection( $opt_name, array(
    'title'            => __( 'Информация', 'redux-framework-demo' ),
    'desc'             => __( '', 'redux-framework-demo' ),
    'id'               => 'basic-Text',
    'subsection'       => true,
    'customizer_width' => '700px',
    'fields'           => array(
        array(
            'id'       => 'company_name',
            'type'     => 'text',
            'title'    => __( 'Название компании', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'AYEP\'S',
        ),
        array(
            'id'       => 'phone_number',
            'type'     => 'text',
            'title'    => __( 'Номер телефона', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '+7 (495) 999-99-99',
        ),
//        array(
//            'id'       => 'phone_number_bottom',
//            'type'     => 'text',
//            'title'    => __( 'Номер телефона нижний', 'redux-framework-demo' ),
//            'subtitle' => __( '', 'redux-framework-demo' ),
//            'desc'     => __( '', 'redux-framework-demo' ),
//            'default'  => '+7 (495) 999-99-99',
//        ),
        array(
            'id'       => 'faks',
            'type'     => 'text',
            'title'    => __( 'Факс', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'email',
            'type'     => 'text',
            'title'    => __( 'Email', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'vlad@ayeps.com',
        ),
        array(
            'id'       => 'email_2',
            'type'     => 'text',
            'title'    => __( 'Email 2', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'vlad@ayeps.com',
        ),
        array(
            'id'       => 'skype',
            'type'     => 'text',
            'title'    => __( 'Skype', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'address',
            'type'     => 'text',
            'title'    => __( 'Адрес', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'Москва, ул. хромова 20',
        ),
        array(
            'id'     => 'address_map_coord',
            'type'   => 'ayp_map',
            'notice' => false,
            'title'  => __( 'Карта', 'redux-framework-demo' ),
            'desc'   => __( '', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'work_hours',
            'type'     => 'text',
            'title'    => __( 'Часы работы', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'stock_end',
            'type'     => 'date',
            'title'    => __( 'Окончание акции', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'agreement_url',
            'type'     => 'text',
            'title'    => __( 'Ссылка на пользовательское соглашение', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '/agreement',
        ),
        array(
            'id'       => 'instagram_login',
            'type'      => 'text',
            'title'     => 'Инстаграм логин',
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'cruche_centr',
        )
    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Загрузки', 'redux-framework-demo' ),
    'id'    => 'media',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Логотип', 'redux-framework-demo' ),
    'id'         => 'media-media',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'main_logo',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( 'Логотип', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Загрузить изображение логотипа', 'redux-framework-demo' ),
            'subtitle' => __( 'Загрузить изображение логотипа', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
        ),
        array(
            'id'       => 'main_logo_x2',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( 'Логотип Retina', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Загрузить изображение логотипа', 'redux-framework-demo' ),
            'subtitle' => __( 'Изображение с для Retina( 2x размер исходного)', 'redux-framework-demo' ),
            'default'  => array( 'url' => '' ),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Header', 'redux-framework-demo' ),
    'id'    => 'header',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture'
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Настройки Хедера', 'redux-framework-demo' ),
    'id'         => 'header-header',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'logo_text',
            'type'     => 'editor',
            'title'    => __( 'Текст логотипа', 'redux-framework-demo' ),
            'subtitle' => __( 'Текст логотипа', 'redux-framework-demo' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Соц-сети', 'redux-framework-demo' ),
    'id'    => 'social',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture'
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Ссылки на соц-сети', 'redux-framework-demo' ),
    'desc'             => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/text/" target="_blank">docs.reduxframework.com/core/fields/text/</a>',
    'id'               => 'social-Text',
    'subsection'       => true,
    'customizer_width' => '700px',
    'fields'           => array(
        array(
            'id'       => 'vk',
            'type'     => 'text',
            'title'    => __( 'Вконтакте', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://vk.com/',
        ),
        array(
            'id'       => 'fb',
            'type'     => 'text',
            'title'    => __( 'Facebook', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://www.facebook.com/',
        ),
        array(
            'id'       => 'instagram',
            'type'     => 'text',
            'title'    => __( 'Инстаграм', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://www.instagram.com/',
        ),
        array(
            'id'       => 'twitter',
            'type'     => 'text',
            'title'    => __( 'Twitter', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://www.twitter.com/',
        ),
        array(
            'id'       => 'google_plus',
            'type'     => 'text',
            'title'    => __( 'Google+', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://plus.google.com/?hl=ru',
        ),
        array(
            'id'       => 'youtube',
            'type'     => 'text',
            'title'    => __( 'Youtube', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://youtube.com',
        ),
        array(
            'id'       => 'ok',
            'type'     => 'text',
            'title'    => __( 'Одноклассники', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => 'https://ok.ru',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Уведомления', 'redux-framework-demo' ),
    'id'    => 'mail',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture'
) );
Redux::setSection( $opt_name, array(
    'title' => __( 'Настройки почты', 'redux-framework-demo' ),
    'id'    => 'mail-options',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture',
    'subsection'       => true,
    'fields'           => array(
        array(
            'id'       => 'mail_to',
            'type'     => 'text',
            'title'    => __( 'Адрес получателя', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( 'можно указывать несколько получателей через запятую', 'redux-framework-demo' ),
            'default'  => 'info@'.str_replace(array( 'http://', 'https://' ), '', site_url()),
        ),
        array(
            'id'       => 'mail_from',
            'type'     => 'text',
            'title'    => __( 'Адрес отправителя', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( 'заголовок "от кого"', 'redux-framework-demo' ),
            'default'  => 'no-reply@'.str_replace(array( 'http://', 'https://' ), '', site_url()),
        ),
        array(
            'id'       => 'mail_from_name',
            'type'     => 'text',
            'title'    => __( 'Имя отправителя', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( 'заголовок "от кого"', 'redux-framework-demo' ),
            'default'  => get_bloginfo('name'),
        ),
        array(
            'id'       => 'mail_smtp',
            'type'     => 'checkbox',
            'title'    => __( 'Использовать SMTP', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '0',
        ),
        array(
            'id'       => 'mail_smtp_server',
            'type'     => 'text',
            'title'    => __( 'SMTP сервер', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'required' => array(
                array('mail_smtp','equals','1'),
            ),
            'default'  => '',
        ),
        array(
            'id'       => 'mail_smtp_login',
            'type'     => 'text',
            'title'    => __( 'SMTP логин', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'required' => array(
                array('mail_smtp','equals','1'),
            ),
            'default'  => '',
        ),
        array(
            'id'       => 'mail_smtp_pass',
            'type'     => 'password',
            'title'    => __( 'SMTP пароль', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'required' => array(
                array('mail_smtp','equals','1'),
            ),
            'default'  => '',
        ),
        array(
            'id'       => 'mail_smtp_port',
            'type'     => 'text',
            'title'    => __( 'SMTP порт', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'required' => array(
                array('mail_smtp','equals','1'),
            ),
            'default'  => '465',
        ),
        array(
            'id'       => 'mail_smtp_secure',
            'type'     => 'text',
            'title'    => __( 'SMTP Secure', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'required' => array(
                array('mail_smtp','equals','1'),
            ),
            'default'  => '',
        ),
    )
) );
Redux::setSection( $opt_name, array(
    'title' => __( 'Настройки СМС', 'redux-framework-demo' ),
    'id'    => 'sms-options',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture',
    'subsection'       => true,
    'fields'           => array(
        array(
            'id'       => 'sms_to',
            'type'     => 'text',
            'title'    => __( 'Номер телефона', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( 'можно указывать несколько получателей через запятую', 'redux-framework-demo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'sms_api',
            'type'     => 'text',
            'title'    => __( 'СМС API', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Аналитика', 'redux-framework-demo' ),
    'id'    => 'analytics-options',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture',
    'subsection'       => false,
    'fields'           => array(
        array(
            'id'       => 'metrika',
            'title'    => __( 'Код Google Аналитики, Яндекс метрики', 'redux-framework-demo' ),
            'type'     => 'ace_editor',
            'subtitle' => __( 'Paste your JS code here.', 'redux-framework-demo' ),
            'mode'     => 'javascript',
            'default'  => '',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title' => __( 'Отключение встроенных функций', 'redux-framework-demo' ),
    'id'    => 'remove-options',
    'desc'  => __( '', 'redux-framework-demo' ),
    'icon'  => 'el el-picture',
    'subsection'       => false,
    'fields'           => array(
        array(
            'id'       => 'remove_rss',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить Rss Feed', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '1',
        ),
        array(
            'id'       => 'remove_rsd',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить Rsd', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '1',
        ),
        array(
            'id'       => 'remove_rest',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить Rest API', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '0',
        ),
        array(
            'id'       => 'remove_emoji',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить Emoji', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '1',
        ),
        array(
            'id'       => 'remove_rel_canonical',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить Rel Canonical', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '1',
        ),
        array(
            'id'       => 'remove_wp_generator',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить WP Generator', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '1',
        ),
        array(
            'id'       => 'remove_xmlrpc',
            'type'     => 'checkbox',
            'title'    => __( 'Отключить XMLRPC', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => '1',
        ),
    )
) );

if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
    $section = array(
        'icon'   => 'el el-list-alt',
        'title'  => __( 'Documentation', 'redux-framework-demo' ),
        'fields' => array(
            array(
                'id'       => '17',
                'type'     => 'raw',
                'markdown' => true,
                'content_path' => dirname( __FILE__ ) . '/../README.md', // FULL PATH, not relative please
                //'content' => 'Raw content here',
            ),
        ),
    );
    Redux::setSection( $opt_name, $section );
}
/*
 * <--- END SECTIONS
 */


/*
 *
 * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
 *
 */

/*
*
* --> Action hook examples
*
*/

// If Redux is running as a plugin, this will remove the demo notice and links
//add_action( 'redux/loaded', 'remove_demo' );

// Function to test the compiler hook and demo CSS output.
// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
//add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

// Change the arguments after they've been declared, but before the panel is created
//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

// Change the default value of a field after it's been set, but before it's been useds
//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

// Dynamically add a section. Can be also used to modify sections/fields
//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

/**
 * This is a test function that will let you see when the compiler hook occurs.
 * It only runs if a field    set with compiler=>true is changed.
 * */
if ( ! function_exists( 'compiler_action' ) ) {
    function compiler_action( $options, $css, $changed_values ) {
        echo '<h1>The compiler hook has run!</h1>';
        echo "<pre>";
        print_r( $changed_values ); // Values that have changed since the last save
        echo "</pre>";
        //print_r($options); //Option values
        //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
    }
}

/**
 * Custom function for the callback validation referenced above
 * */
if ( ! function_exists( 'redux_validate_callback_function' ) ) {
    function redux_validate_callback_function( $field, $value, $existing_value ) {
        $error   = false;
        $warning = false;

        //do your validation
        if ( $value == 1 ) {
            $error = true;
            $value = $existing_value;
        } elseif ( $value == 2 ) {
            $warning = true;
            $value   = $existing_value;
        }

        $return['value'] = $value;

        if ( $error == true ) {
            $field['msg']    = 'your custom error message';
            $return['error'] = $field;
        }

        if ( $warning == true ) {
            $field['msg']      = 'your custom warning message';
            $return['warning'] = $field;
        }

        return $return;
    }
}

/**
 * Custom function for the callback referenced above
 */
if ( ! function_exists( 'redux_my_custom_field' ) ) {
    function redux_my_custom_field( $field, $value ) {
        print_r( $field );
        echo '<br/>';
        print_r( $value );
    }
}

/**
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 * */
if ( ! function_exists( 'dynamic_section' ) ) {
    function dynamic_section( $sections ) {
        //$sections = array();
        $sections[] = array(
            'title'  => __( 'Section via hook', 'redux-framework-demo' ),
            'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
            'icon'   => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }
}

/**
 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
 * */
if ( ! function_exists( 'change_arguments' ) ) {
    function change_arguments( $args ) {
        //$args['dev_mode'] = true;

        return $args;
    }
}

/**
 * Filter hook for filtering the default value of any given field. Very useful in development mode.
 * */
if ( ! function_exists( 'change_defaults' ) ) {
    function change_defaults( $defaults ) {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }
}

/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if ( ! function_exists( 'remove_demo' ) ) {
    function remove_demo() {
        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_filter( 'plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2 );

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
        }
    }
}

