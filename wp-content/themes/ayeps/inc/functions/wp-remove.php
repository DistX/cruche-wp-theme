<?php

$ayp_redux = $app->get('options');


if($ayp_redux['remove_rss'] == 1){
    aypRemoveFeedLinks();
}
if($ayp_redux['remove_rsd'] == 1){
    aypRemoveRsd();
}
if($ayp_redux['remove_rest'] == 1){
    aypRemoveRestApi();
}
if($ayp_redux['remove_xmlrpc'] == 1){
    add_filter( 'xmlrpc_enabled', '__return_false' );
}
if($ayp_redux['remove_emoji'] == 1){
    aypRemoveEmoji();
}
if($ayp_redux['remove_rel_canonical'] == 1){
    aypRemoveRelCanonical();
}
if($ayp_redux['remove_wp_generator'] == 1){
    aypRemoveWpGenerator();
}


remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );


// если собираетесь выводить вставки из других сайтов на своем, то закомментируйте след. строку.
//remove_action( 'wp_head',                'wp_oembed_add_host_js'                 );


function aypRemoveRelCanonical(){
    remove_action('wp_head', 'rel_canonical');
}

function aypRemoveFeedLinks(){
    remove_action( 'wp_head', 'feed_links', 2 );
}
function aypRemoveWpGenerator(){
    remove_action('wp_head', 'wp_generator');
    add_filter( 'revslider_meta_generator', 'remove_revslider_meta_tag' );
    add_action('init', 'aypVCgeneratorRemove', 100);
}

function aypRemoveEmoji(){
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    add_filter( 'emoji_svg_url', '__return_false' );
}

function aypRemoveRsd(){
    remove_action ('wp_head', 'rsd_link');
}

function aypRemoveRestApi(){

    remove_action( 'wp_head', 'wp_shortlink_wp_head');
    add_filter('rest_enabled', '__return_false');
    remove_action( 'wp_head', 'wlwmanifest_link');

    // Отключаем Embeds связанные с REST API
    remove_action( 'rest_api_init',          'wp_oembed_register_route'              );
    remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10 );

    // Отключаем фильтры REST API
    remove_action( 'xmlrpc_rsd_apis',            'rest_output_rsd' );
    remove_action( 'wp_head',                    'rest_output_link_wp_head', 10 );
    remove_action( 'template_redirect',          'rest_output_link_header', 11 );
    remove_action( 'auth_cookie_malformed',      'rest_cookie_collect_status' );
    remove_action( 'auth_cookie_expired',        'rest_cookie_collect_status' );
    remove_action( 'auth_cookie_bad_username',   'rest_cookie_collect_status' );
    remove_action( 'auth_cookie_bad_hash',       'rest_cookie_collect_status' );
    remove_action( 'auth_cookie_valid',          'rest_cookie_collect_status' );
    remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );

// Отключаем события REST API
    remove_action( 'init',          'rest_api_init' );
    remove_action( 'rest_api_init', 'rest_api_default_filters', 10 );
    remove_action( 'parse_request', 'rest_api_loaded' );
}


function aypVCgeneratorRemove() {
	if(function_exists('visual_composer'))
        remove_action('wp_head', array(visual_composer(), 'addMetaData'));
}

function remove_revslider_meta_tag() {
    return '';
}
