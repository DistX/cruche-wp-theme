<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'init', 'ayp_register_post_types' );
function ayp_register_post_types(){
    register_post_type( 'ayp_team',
        array(
            'labels' => array(
                'name'          => __( 'Специалисты' ),
                'singular_name' => __( 'Специалист' ),
                'all_items'     => __( 'Все Специалисты' ),
                'add_new'       => __( 'Добавить Специалиста' ),
                'add_new_item'  => __( 'Добавить нового Специалиста' ),
                'edit_item'     => __( 'Редактировать' ),
            ),
            'exclude_from_search' => false,
            'has_archive'         => true,
            'public'              => true,
            'rewrite'             => true,
            'show_in_nav_menus'   => false,
            'show_ui'             => true,
            'supports'            => array('title', 'editor', 'thumbnail', 'custom-fields'),

        )
    );
    register_post_type( 'ayp_testimonial',
        array(
            'labels' => array(
                'name'          => __( 'Testimonials', 'ayeps' ),
                'singular_name' => __( 'Testimonials', 'ayeps' ),
                'all_items'     => __( 'All Testimonials', 'ayeps' ),
                'add_new'       => __( 'Add Testimonial', 'ayeps' ),
                'add_new_item'  => __( 'Add New Testimonial', 'ayeps' ),
                'edit_item'     => __( 'Edit Testimonial', 'ayeps' ),
            ),
            'exclude_from_search' => false,
            'has_archive'         => true,
            'public'              => true,
            'rewrite'             => true,
            'show_in_nav_menus'   => false,
            'show_ui'             => true,
            'supports'            => array('title', 'editor', 'thumbnail', 'custom-fields'),
            'menu_icon'           => 'dashicons-format-status'
        )
    );
    register_post_type( 'ayp_services',
        array(
            'labels' => array(
                'name'          => __( 'Услуги', 'ayeps' ),
                'singular_name' => __( 'Услуга', 'ayeps' ),
                'all_items'     => __( 'Все Услуги', 'ayeps' ),
                'add_new'       => __( 'Добавить услугу', 'ayeps' ),
                'add_new_item'  => __( 'Добавить услугу', 'ayeps' ),
                'edit_item'     => __( 'Редактировать', 'ayeps' ),
            ),
            'exclude_from_search' => false,
            'has_archive'         => true,
            'public'              => true,
            'rewrite'             => true,
            'show_in_nav_menus'   => false,
            'show_ui'             => true,
            'supports'            => array('title', 'editor', 'thumbnail', 'custom-fields')
        )
    );
    register_post_type( 'ayp_stock',
        array(
            'labels' => array(
                'name'          => __( 'Акции', 'ayeps' ),
                'singular_name' => __( 'Акция', 'ayeps' ),
                'all_items'     => __( 'Все Акции', 'ayeps' ),
                'add_new'       => __( 'Добавить акцию', 'ayeps' ),
                'add_new_item'  => __( 'Добавить акцию', 'ayeps' ),
                'edit_item'     => __( 'Редактировать акцию', 'ayeps' ),
            ),
            'exclude_from_search' => false,
            'has_archive'         => true,
            'public'              => true,
            'rewrite'             => true,
            'show_in_nav_menus'   => false,
            'show_ui'             => true,
            'supports'            => array('title', 'editor', 'thumbnail', 'custom-fields')
        )
    );

    register_post_type( 'ayp_price',
        array(
            'labels' => array(
                'name'          => __( 'Price', 'ayeps' ),
                'singular_name' => __( 'Price', 'ayeps' ),
                'all_items'     => __( 'All Prices', 'ayeps' ),
                'add_new'       => __( 'Add Price', 'ayeps' ),
                'add_new_item'  => __( 'Add New Price', 'ayeps' ),
                'edit_item'     => __( 'Edit Price', 'ayeps' ),
            ),
            'exclude_from_search' => false,
            'has_archive'         => true,
            'public'              => true,
            'rewrite'             => true,
            'show_in_nav_menus'   => false,
            'show_ui'             => true,
            'supports'            => array('title', 'custom-fields')
        )
    );

	register_post_type( 'ayp_order',
		array(
			'labels' => array(
				'name'          => __( 'Заявки' ),
				'singular_name' => __( 'Заявка' ),
				'all_items'     => __( 'Все Заявки' ),
				'edit_item'     => __( 'Редактировать Заявку' ),
			),
			'exclude_from_search' => true,
			'has_archive'         => false,
			'public'              => false,
			'rewrite'             => false,
			'show_in_nav_menus'   => false,
			'show_ui'             => true,
			'supports'            => array('title', 'editor'),
			'menu_icon'           => 'dashicons-email-alt'
		)
	);
}

add_action( 'init', 'ayp_register_taxonomies' );
function ayp_register_taxonomies(){
    //Custom taxonomies
}

function ayp_add_meta(){
    add_meta_box( 'ayp_reviews_meta_id', 'Настройки', 'ayp_page_meta_callback', 'page' );

    add_meta_box( 'ayp_price_meta', 'Прайс лист', 'ayp_price_meta_callback', 'ayp_price' );
}
add_action('add_meta_boxes', 'ayp_add_meta');
function ayp_page_meta_callback(){
    wp_nonce_field( plugin_basename(__FILE__), 'ayp_product_nonce' );
    $post_id = get_the_ID();
    if ( ! did_action( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }

    ?>
    <div>
        <label for="">Галлерея</label><br>
        <div id="listingimagediv">
            <?php
            $gallery = get_post_meta( $post_id, 'ayp_gallery', true );
            $old_content_width = $content_width;
            $content_width = 254;
            if ( $gallery ) {
                $arr = unserialize($gallery);
                echo '<p class="hide-if-no-js"><a title="' . esc_attr__( 'Задать Галлерею', 'text-domain' ) . '" href="javascript:;" data-remove-id="remove_ayp_floor_image_attach" data-parent="listingimagediv" id="upload_ayp_floor_image_attach" data-input-id="ayp_floor_image_attach" data-uploader_title="' . esc_attr__( 'Choose an image', 'text-domain' ) . '" data-uploader_button_text="' . esc_attr__( 'Задать Галлерею', 'text-domain' ) . '">' . esc_html__( 'Задать Галлерею', 'text-domain' ) . '</a></p>';
                echo '<div class="listingimagediv__inner">';
                foreach ($arr as $a){

                    ?>
                    <div class="ayp_gallery-preview"><img class="ayp_gallery-img" src="<?php echo wp_get_attachment_image_src($a)[0]?>" ></div>
                    <input id="myplugin-image-input<?php echo $a?>" type="hidden" name="ayp_gallery[]"  value="<?php echo $a?>">
                    <?php
                }
                echo '</div>';

            } else {
                $content = '<p class="hide-if-no-js"><a title="' . esc_attr__( 'Задать Галлерею', 'text-domain' ) . '" href="javascript:;" data-remove-id="remove_ayp_floor_image_attach" data-parent="listingimagediv" id="upload_ayp_floor_image_attach" data-input-id="ayp_floor_image_attach" data-uploader_title="' . esc_attr__( 'Choose an image', 'text-domain' ) . '" data-uploader_button_text="' . esc_attr__( 'Задать Галлерею', 'text-domain' ) . '">' . esc_html__( 'Задать Галлерею', 'text-domain' ) . '</a></p>';
                $content .= '<div class="listingimagediv__inner"></div>';
                echo $content;
            }

            ?>
        </div>
    </div>
    <style>
        .ayp_gallery-img{
            max-width: 100%;
        }
        .ayp_gallery-preview{
            display: inline-block;
            width:100px;
            height:100px;
        }
    </style>

    <script>
        jQuery(document).ready(function($) {

            // Uploading files
            var file_frame;

            function upload_listing_image( button ) {
                var button_id = button.attr('id');
                var field_id = button.data('input-id');
                var parent =  button.data('parent');
                var remove_id = button.data('remove-id');
                console.log(field_id);
                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    file_frame.open();
                    return;
                }

                // Create the media frame.
                file_frame = wp.media.frames.file_frame = wp.media({
                    title: "Загрузка изображений",
                    button: {
                        text: jQuery( this ).data( 'uploader_button_text' ),
                    },
                    multiple: true
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    var attachments = file_frame.state().get('selection').map(

                    function( attachment ) {

                        attachment.toJSON();
                        return attachment;

                    });
                    var append = '';
                    for ( var i = 0; i < attachments.length; ++i) {
                        append += '<div class="ayp_gallery-preview"><img class="ayp_gallery-img" src="' + attachments[i].attributes.url + '" ><input type="hidden" name="ayp_gallery[]" value="' + attachments[i].id + '"></div>';
                        //sample function 2: add hidden input for each image
                        $('#'+parent).find('.listingimagediv__inner').html(
                            append
                        );
                    }
                    button.text( 'Изменить' );
                });

                // Finally, open the modal
                file_frame.open();
            };

            jQuery('#listingimagediv').on( 'click', '#upload_ayp_floor_image_attach', function( event ) {
                event.preventDefault();
                upload_listing_image( jQuery(this) );
            });

        });
        jQuery(document).ready( function( $ ) {

            jQuery('#upload_image_button').click(function() {

                window.send_to_editor = function(html) {
                    imgurl = jQuery(html).attr('src')
                    jQuery('#ayp_floor_plan').val(imgurl);
                    // jQuery('#picsrc').attr("src",imgurl);
                    tb_remove();
                }

                formfield = jQuery('#ayp_floor_plan').attr('name');
                tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
                return false;
            });


        });
    </script>
    <?php
}

add_action( 'save_post', 'ayp_save_custom' );
function ayp_save_custom($post_id){
    // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
    if ( ! wp_verify_nonce( $_POST['ayp_product_nonce'], plugin_basename(__FILE__) ) )
        return $post_id;

    // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;


    $gallery = serialize($_POST['ayp_gallery']);

    $ayp_price['titles'] = $_POST['price_title'];
    $ayp_price['values'] = $_POST['price_value'];

    update_post_meta( $post_id, 'ayp_gallery', $gallery );
    update_post_meta( $post_id, 'ayp_price_table', serialize($ayp_price));
}

function ayp_price_meta_callback(){
    $post_id = get_the_ID();
    wp_nonce_field( plugin_basename(__FILE__), 'ayp_product_nonce' );
    $ayp_price_table = get_post_meta( $post_id, 'ayp_price_table', true );
    $prices_titles = unserialize($ayp_price_table)['titles'];
    $prices_values = unserialize($ayp_price_table)['values'];
    ?>
    <style>
        .price_row{
            display: flex;
            margin-bottom:15px;
        }
        .price__col{
            flex:1;
            padding:0 15px;
        }
        .price__col input{
            width:100%;
        }
    </style>
    <div id="price-meta-container">
        <?php if($ayp_price_table):
            for($i=0;$i<count($prices_titles);$i++){
                ?>
                <div class="price_row">
                    <div class="price__col"><input type="text" value="<?php echo $prices_titles[$i]?>" name="price_title[]"></div>
                    <div class="price__col"><input type="text" value="<?php echo $prices_values[$i]?>" name="price_value[]"></div>
                    <button class="js-remove-row">Удалить</button>
                </div>
                <?php
            }
            ?>
        <?php endif;?>
        <div class="price_row">
            <div class="price__col"><input type="text" name="price_title[]"></div>
            <div class="price__col"><input type="text" name="price_value[]"></div>
            <button class="js-remove-row">Удалить</button>
        </div>
    </div>
    <a href="#" class="btn js-add-price-row">Добавить Строку</a>
    <script>
        jQuery(document).ready(function ($) {
            $('.js-add-price-row').click(function (e) {
                e.preventDefault();
                $('#price-meta-container').append('<div class="price_row">' +
                    '<div class="price__col"><input type="text" name="price_title[]"></div>' +
                    '<div class="price__col"><input type="text" name="price_value[]"></div>' +
                        '<button class="js-remove-row">Удалить</button>' +
                    '</div>');
            });
            $('#price-meta-container').on('click', '.js-remove-row', function (e) {
                e.preventDefault();
                $(this).parents('.price_row').remove();
            });
        });
    </script>
<?php
}