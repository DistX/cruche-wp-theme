<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'wp_enqueue_scripts', 'ayp_theme_scripts' );
function ayp_theme_scripts() {
	/**
	 * Enqueue Styles
	 */

	// Font Awesome CSS
	wp_enqueue_style( 'ayp_font-awesome', AYP_THEME_URI . '/assets/fonts/font-awesome/css/font-awesome.min.css' );

	// Theme Main CSS
	wp_enqueue_style( 'ayp_main-style', AYP_THEME_URI  . '/dist/index.min.css', '', time() );

	

	/**
	 * Enqueue Scripts
	 */

	// Theme Main JavaScript
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'manifest', AYP_THEME_URI . '/dist/manifest.min.js', array(), time(), true );
    wp_enqueue_script( 'ayp_main-scripts', AYP_THEME_URI . '/dist/index.min.js', array(), time(), true );
}