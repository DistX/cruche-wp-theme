<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function ayp_testimonial_slider($atts){
    extract( shortcode_atts( array(
        'count' => '',
    ), $atts ) );
    $posts = Timber::get_posts(array('post_type' => 'ayp_testimonial','posts_per_page' => -1));
    $data = array();
    $data['posts'] = $posts;
    $data['count'] = $count;
    return Timber::compile( 'shortcode/testimonials/testimonials.twig', $data );
}
add_shortcode('ayp_testimonial_slider','ayp_testimonial_slider');

function ayp_team_slider($atts){
    $posts = Timber::get_posts(array('post_type' => 'ayp_team','posts_per_page' => -1));
    $data = array();
    $data['posts'] = $posts;
    return Timber::compile( 'shortcode/team/slider.twig', $data );

}
add_shortcode('ayp_team_slider','ayp_team_slider');

function ayp_services_slider($atts){
    $posts = Timber::get_posts(array('post_type' => 'ayp_services','posts_per_page' => -1));
    $data = array();
    $data['posts'] = $posts;
    $data['AYP_THEME_URI'] = AYP_THEME_URI;
    return Timber::compile( 'shortcode/services/slider.twig', $data );

}
add_shortcode('ayp_services_slider','ayp_services_slider');

function ayp_creative_slider(){
    global $post;
    $gallery = get_post_meta( $post->ID, 'ayp_gallery', true );
    $data['images'] = array();
    foreach(unserialize($gallery) as $item)
    {
        $data['images'][] = wp_get_attachment_image_src($item,'ayp_creative')[0];
    }

    return Timber::compile( 'shortcode/creative/slider.twig', $data );
}
add_shortcode('ayp_creative_slider', 'ayp_creative_slider');

function priceAccordeon(){
    $content = '';
    $posts = get_posts(array('post_type' => 'ayp_price','posts_per_page' => -1));

    ob_start();
    ?>
    <div class="price-table">
        <?php
            foreach ($posts as $post){
                $ayp_price_table = unserialize(get_post_meta( $post->ID, 'ayp_price_table', true ));
                $prices_titles = $ayp_price_table['titles'];
                $prices_values = $ayp_price_table['values'];

                $data['title'] = $post->post_title;
                $data['prices'] = array();
                for($i=0;$i<count($prices_titles);$i++){
                    $data['prices'][] = array('title'=>$prices_titles[$i],'value'=>$prices_values[$i]);
                }
                echo Timber::compile( 'shortcode/price/accordeon-item.twig', $data );
            }
        ?>
    </div>
    <?php
    return ob_get_clean();
}
add_shortcode('ayp_price_accordeon','priceAccordeon');

function ayp_stock(){
    $posts = Timber::get_posts(array('post_type' => 'ayp_stock','posts_per_page' => -1));
    $data = array();
    $data['posts'] = $posts;
    $data['AYP_THEME_URI'] = AYP_THEME_URI;
    return Timber::compile( 'shortcode/stock/index.twig', $data );
}
add_shortcode('ayp_stock','ayp_stock');

function ayp_lessons(){
    $ayp_lessons = unserialize(get_option( 'ayp_lessons' ));

    return Timber::compile( 'shortcode/lessons/index.twig', array('lessons'=>$ayp_lessons['data']) );
}
add_shortcode('ayp_lessons', 'ayp_lessons');

function ayp_vc_map(){
    vc_map( array(
        'name'                    => __( 'Слайдер отзывов' , 'ayeps' ),
        'base'                    => 'ayp_testimonial_slider',
        "is_container"            => false,
        'icon'                    => AYP_THEME_IMAGES_ADMIN.'/slider-icon.png',
        'show_settings_on_create' => false,
        "category"                => __( "Content", "ayeps"),
        'params'                  => array(
            array(
                "type" => "dropdown",
                "heading" => __("Count of elements", "ayeps"),
                "holder" => "div",
                "class" => "",
                "param_name" => "count",
                'value'       => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'std' => "2",
            ),
        )
    ) );

    if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        //Container
    }
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        //Item
    }
}
add_action( 'vc_before_init', 'ayp_vc_map' );