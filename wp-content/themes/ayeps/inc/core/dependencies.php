<?php

$container = $app->getContainer();

/**
 * @return \Timber\Timber
 */
$container['timber'] = function (){
	$timber = new Timber\Timber();
	$timber::$dirname = array('templates', 'views');

	return $timber;
};
/**
 * @param $c
 *
 * @return \Ayeps\AyepsSite
 */
$container['site'] = function ($c){
	return new Ayeps\AyepsSite($c);
};

/**
 * @return mixed
 */
$container['options'] = function (){
	Redux::init( 'ayp_redux' );
	global $ayp_redux;

	return $ayp_redux;
};

$container['logger'] = function () {
	$logger = new Monolog\Logger('Ayeps');
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$logger->pushHandler(new Monolog\Handler\StreamHandler( __DIR__ . '../../logs/app.log', \Monolog\Logger::DEBUG));
	return $logger;
};


$container['jsonResponce'] = function (){
	$response = new \Symfony\Component\HttpFoundation\JsonResponse();
	return $response;
};