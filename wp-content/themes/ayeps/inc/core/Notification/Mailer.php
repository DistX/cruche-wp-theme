<?php

namespace Ayeps\Notification;


class Mailer
{
	public function __construct($options) {
	    $this->options = $options;
		$this->isSMTP = $options['mail_smtp'];

		$this->SMTPServer = $options['mail_smtp_server'];
		$this->Username = $options['mail_smtp_login'];
		$this->Password = $options['mail_smtp_pass'];
		$this->SMTPSecure = $options['mail_smtp_secure'];
		$this->Port = $options['mail_smtp_port'];

		$this->mail_from = $options['mail_from'];
		$this->mail_from_name = $options['mail_from_name'];
		$this->mail_to = $options['mail_to'];
	}


	public function send($subject, $content)
	{
        return $this->sendMail($subject,$content);
	}

	public function setAddress($to)
	{
		$this->mail_to = $to;
	}


	private function sendMail($subject, $content)
	{
		$headers = "Content-type: text/html; charset=utf-8\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "From: ".$this->mail_from_name." <".$this->mail_from.">\r\n";

		return wp_mail($this->mail_to,$subject,$content,$headers);
	}

	private function sendSMTP($subject, $content)
	{
		$mail = new \PHPMailer();

		$mail->isSMTP();                                        // Set mailer to use SMTP
		$mail->Host = $this->SMTPServer;           // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                                 // Enable SMTP authentication
		$mail->Username = $this->Username;        // SMTP username
		$mail->Password = $this->Password;         // SMTP password
		$mail->SMTPSecure = $this->SMTPSecure;     // Enable TLS encryption, `ssl` also accepted
		$mail->Port = $this->Port;             // TCP port to connect to

		$mail->setFrom($this->mail_from, $this->mail_from_name);

		$to = explode(',', $this->mail_to);
		foreach ($to as $address){
			$mail->addAddress($address);
		}

		$mail->addReplyTo($this->mail_from, $this->mail_from_name);

		$mail->isHTML(true);                           // Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    = $content;

		if(!$mail->send()) {
			return true;
		} else {
			return false;
		}
	}
}