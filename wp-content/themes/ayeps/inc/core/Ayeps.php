<?php

namespace Ayeps;

use Pimple\Container;

class App
{

	/**
	 * @var array|Container
	 */
	private $container;

	/**
	 * App constructor.
	 *
	 * @param array $containerreturn $obj;
	 */
	public function __construct($container = [])
	{
		if (is_array($container)) {
			$container = new Container($container);
		}
		$this->container = $container;
	}

	/**
	 * @return array|Container
	 */
	public function getContainer()
	{
		return $this->container;
	}


	/**
	 * Get container by id
	 *
	 * @param $method
	 *
	 * @return mixed
	 */
	public function get($method)
	{
		if ($this->container->offsetExists($method)) {
			$obj = $this->container->offsetGet($method);
			return $obj;
		}

		throw new \BadMethodCallException("Method $method is not a valid method");
	}

	/**
	 * @param $method
	 * @param $args
	 *
	 * @return mixed
	 */
	public function __call($method, $args)
	{
		if ($this->container->offsetExists($method)) {
			$obj = $this->container->offsetGet($method);
			if (is_callable($obj)) {
				return call_user_func_array($obj, $args);
			}
		}

		throw new \BadMethodCallException("Method $method is not a valid method");
	}

}