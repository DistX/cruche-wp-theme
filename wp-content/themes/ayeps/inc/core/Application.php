<?php

namespace Ayeps;


use Ayeps\Notification\Mailer;
use Pimple\Container;

class Application
{
	public function __construct($container = [])
	{
        if (is_array($container)) {
            $container = new Container($container);
        }
        $this->container = $container;
        $this->options = $this->get('options');
	}

	public function save($plain_text, $subject, $type = 'ayp_order')
	{
		$id = wp_insert_post(array(
			'post_content'=> $plain_text,
			'post_title' => $subject,
			'post_type' => $type,
			'post_status' => 'publish'
		));

		return $id;
	}

    public function get($method)
    {
        if ($this->container->offsetExists($method)) {
            $obj = $this->container->offsetGet($method);
            return $obj;
        }

        throw new \BadMethodCallException("Method $method is not a valid method");
    }

	public function sendAdminNotification($subject,$content)
	{
		$mailer = $this->getMailer();

		return $mailer->send($subject,$content);
	}

	public function sendUserNotification($subject,$content,$to)
	{
		$mailer = $this->getMailer();
		$mailer->setAddress($to);
		return $mailer->send($subject,$content);
	}

	private function getMailer()
	{
		return new Mailer($this->options);
	}

}