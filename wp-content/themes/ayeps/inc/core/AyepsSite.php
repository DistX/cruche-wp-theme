<?php

namespace Ayeps;

use Pimple\Container;
use Timber\Site as TimberSite;
use Twig\Extension\StringLoaderExtension as Twig_Extension_StringLoader;
use Timber\Menu as TimberMenu;
use Twig\TwigFunction;
use InstagramScraper\Instagram;


/**
 * Class AyepsSite
 * @package Ayeps
 */
class AyepsSite extends TimberSite{

	/**
	 * AyepsSite constructor.
	 *
	 * @param Container $c
	 */
	function __construct(Container $c) {

		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_action('wp_footer', array( $this, 'ayp_action_javascript' ), 99); // для фронта
		$this->options = $c->offsetGet('options');
		parent::__construct();
	}

	/**
	 * @param $context
	 *
	 * @return mixed
	 */
	function add_to_context( $context ) {

		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		$context['ayp_options'] = $this->options;
		$context['AYP_THEME_URI'] = AYP_THEME_URI;

        $cachedMedia = get_transient('instagram_medias');

        if(empty($cachedMedia)){
            try{
                $instaMediasC = new Instagram();
                $instaMedias = $instaMediasC->getMedias($this->options['instagram_login'], 10);
                set_transient( 'instagram_medias', $instaMedias, 60*60*6 );
                $context['medias'] = $instaMedias;
            }catch(Exception $e){
                $context['medias'] = '';

            }
        }
        else{
            $context['medias'] = $cachedMedia;
        }

		return $context;
	}

	/**
	 * @param $twig
	 *
	 * @return mixed
	 */
	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFunction( new TwigFunction('dumper',function ($var){
			dump($var);
		}));
		return $twig;
	}

    /**
     * Добавить на страницу функцию рендера яндекс карты
     */
    function ayp_action_javascript() {
	    $options = $this->options;
        $coord = $options['address_map_coord'];
        ?>
        <script type="text/javascript" >
            var mapCenter = '<?php echo $coord;?>',
                adresses= '<?php echo $coord;?>';
            var myMap;
            function mapInit(){
                var coord = mapCenter;
                ymaps.ready(function () {
                    var placeMarks = [];
                    var myPlacemark = new ymaps.Placemark(adresses.split(','), {
                        hintContent: '<?php echo addslashes($options['company_name'])?>',
                        balloonContent: '<?php echo addslashes($options['address'])?>'
                    },{
                        iconLayout: 'default#image',
                        iconImageSize: [48,76],
                        iconImageHref: '<?php echo AYP_THEME_URI;?>/dist/map-marker.png',
                        iconImageOffset: [-24, -76]
                    });
                    placeMarks.push(myPlacemark);

                    myMap = new ymaps.Map('map', {
                        center: mapCenter.split(','),
                        zoom: 15,
                        controls: []
                    }, {
                        searchControlProvider: 'yandex#search'
                    });
                    myMap.behaviors.disable('scrollZoom');
                    placeMarks.forEach(function(item, i, arr){
                        myMap.geoObjects.add(item);
                    });
                });
            }
            function moveMap(c) {
                myMap.setCenter(c);
            }
        </script>
        <script async="" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=mapInit" type="text/javascript"></script>
        <?php
    }

}