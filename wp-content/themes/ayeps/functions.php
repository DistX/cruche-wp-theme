<?php
/**
 * Define constants
 */
define('AYP_THEME_VER', '0.1');
define('AYP_THEME_URI', get_template_directory_uri() );
define('AYP_THEME_IMAGES', AYP_THEME_URI . '/assets/images');
define('AYP_THEME_IMAGES_ADMIN', AYP_THEME_URI . '/assets/images/admin-panel');
define('AYP_THEME_SRC_CSS', AYP_THEME_URI . '/assets/src/css');
define('AYP_THEME_SRC_JS', AYP_THEME_URI . '/assets/src/js');
define('AYP_THEME_DIST_CSS', AYP_THEME_URI . '/assets/dist/css');
define('AYP_THEME_DIST_JS', AYP_THEME_URI . '/assets/dist/js');
define('AYP_THEME_INC', AYP_THEME_URI . '/inc');


require_once(__DIR__ . '/vendor/autoload.php');
/**
 * Redux Framework
 */
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/framework/ReduxCore/framework.php' ) ) {
	require_once( dirname( __FILE__ ) . '/framework/ReduxCore/framework.php' );
}
if ( !isset( $ayp_redux ) && file_exists( dirname( __FILE__ ) . '/framework/basic-config.php' ) ) {
	require_once( dirname( __FILE__ ) . '/framework/basic-config.php' );
}

require_once (__DIR__ . '/inc/core/Ayeps.php');

$app = new Ayeps\App();

require_once (__DIR__ . '/inc/core/dependencies.php');

$timber = $app->get('timber');

$app->get('site');


add_image_size('ayp_services', 350, 350, array('center','center'));
add_image_size('ayp_services_x2', 700, 700, array('center','center'));

add_image_size('ayp_creative', 585,585, array('left','center'));
add_image_size('ayp_team', 306,420, false);

add_image_size('ayp_stock',494,229,array('center','center'));
add_image_size('ayp_stock_x2',988,458,array('center','center'));

add_theme_support( 'post-formats' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'custom-header' );
add_theme_support( 'custom-background' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'customize-selective-refresh-widgets' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );



function theme_js_var(){
    ?>
    <script>
        var ayeps = <?php echo json_encode( array(
            'ajax' =>  admin_url( "admin-ajax.php" ),
            'url' => AYP_THEME_URI,
            'nonce' =>  wp_create_nonce( "itr_ajax_nonce" )
        ) ) ?>;
    </script>
    <?php
}
add_action ( 'wp_head', 'theme_js_var' );


/**
 * Include Theme Styles And Scripts
 */
require_once( 'inc/functions/theme-scritps.php' );

/**
 * Include Theme Custom Posts
 */
require_once( 'inc/functions/custom-posts.php' );

/**
 * Include Theme Plugins Setup
 */
require_once( 'inc/functions/plugins.php' );

/**
 * Include Theme Shortcodes
 */
require_once( 'inc/functions/shortcodes.php' );

/**
 * Include Wordpress Remove Actions
 */
require_once( 'inc/functions/wp-remove.php' );

/**
 * Theme Translation
 */
add_action( 'after_setup_theme', 'ayp_theme_setup' );
function ayp_theme_setup(){
	load_theme_textdomain( 'ayeps', AYP_THEME_URI . '/languages' );
}

/**
 * Content Width
 */
if (!isset( $content_width )) $content_width = 1000;

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Widget Area', 'ayeps' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'ayeps' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

/**
 * Register Theme Menus
 */
register_nav_menus(array(
	'main'    => __('Main menu', 'ayeps'),
));


add_action('wp_ajax_ayp_js_callback', 'ayp_js_callback');
add_action('wp_ajax_nopriv_ayp_js_callback', 'ayp_js_callback');
function ayp_js_callback(){
    $errorArray = $message = $response = array();
    $error = false;
    $message['title'] = 'Спасибо';
    $message['subtitle'] = 'Ваша заявка отправлена';

    $phone = purifyInput($_POST['phone']);
    if (empty($phone)){
        $phoneErr = "Пожалуйста, укажите номер телефона";
        $error = true;
        $errorArray['phone'] = array(
            'field' => 'phone',
            'message' => $phoneErr
        );
    }
    else{
        if (!preg_match("/^[+0-9-()\s]*$/",$phone)) {
            $phoneErr = "Номер телефона должен содержать только цифры";
            $error = true;
            $errorArray['phone'] = array(
                'field' => 'phone',
                'message' => $phoneErr
            );
        }
    }
    if($error){
        $response['status'] = 'error';
        $response['error_list'] = $errorArray;
        wp_send_json($response);
    } else{
        global $ayp_redux;
        $mailer = new \Ayeps\Notification\Mailer($ayp_redux);
        $subject = "Заявка на обратный звонок";
        $plain_text = "Написал(а):".$name."\nТелефон: ".$phone."\nEmail: ".$email;
        saveApplication($plain_text,$subject);
        $rows = array(
            array(
                'name' => 'Заявка на обратный звонок',
                'value' => ' - '
            ),
            array(
                'name' => 'Телефон',
                'value' => $phone
            ),
        );
        $template = Timber::compile('email/admin/new-order.twig', array('rows'=>$rows));
        $response['status'] = 'success';
        $response['message'] = $message;

        $mailer->send($subject,$template);
        wp_send_json($response);
    }


}

add_action('wp_ajax_ayp_js_lesson', 'ayp_js_lesson');
add_action('wp_ajax_nopriv_ayp_js_lesson', 'ayp_js_lesson');
function ayp_js_lesson(){
    $errorArray = $message = $response = array();
    $error = false;
    $message['title'] = 'Спасибо';
    $message['subtitle'] = 'Ваша заявка отправлена';

    $name = purifyInput($_POST['name']);
    $phone = purifyInput($_POST['phone']);
    $email = purifyInput($_POST['email']);
    $services = purifyInput($_POST['services']);
    if (empty($name)){
        $nameErr = "Пожалуйста, укажите ваше имя";
        $error = true;
        $errorArray['name'] = array(
            'field' => 'name',
            'message' => $nameErr
        );
    }
    if (empty($phone)){
        $phoneErr = "Пожалуйста, укажите номер телефона";
        $error = true;
        $errorArray['phone'] = array(
            'field' => 'phone',
            'message' => $phoneErr
        );
    }
    else{
        if (!preg_match("/^[+0-9-()\s]*$/",$phone)) {
            $phoneErr = "Номер телефона должен содержать только цифры";
            $error = true;
            $errorArray['phone'] = array(
                'field' => 'phone',
                'message' => $phoneErr
            );
        }
    }
    if (empty($email)) {
        $emailErr = "Пожалуйста, укажите ваш email";
        $error = true;
        $errorArray['email'] = $emailErr;
    } else {
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Указан некорректный адрес почты";
            $error = true;
            $errorArray['email'] = array(
                'field' => 'email',
                'message' => $emailErr
            );
        }
    }
    if($error){
        $response['status'] = 'error';
        $response['error_list'] = $errorArray;
        wp_send_json($response);
    }

    else{
        global $ayp_redux;
        $mailer = new \Ayeps\Notification\Mailer($ayp_redux);
        $subject = "Заявка на обучение";
        $plain_text = "Написал(а):".$name."\nТелефон: ".$phone."\nEmail: ".$email."\nИнтересующие услуги: ".$services;
        saveApplication($plain_text,$subject);
        $rows = array(
            array(
                'name' => 'Заявка на обучение',
                'value' => ' - '
            ),
            array(
                'name' => 'имя',
                'value' => $name
            ),
            array(
                'name' => 'Email',
                'value' => $email
            ),
            array(
                'name' => 'Телефон',
                'value' => $phone
            ),
            array(
                'name' => 'Интересующие услуги',
                'value' => $services
            )
        );
        $template = Timber::compile('email/admin/new-order.twig', array('rows'=>$rows));
        $response['status'] = 'success';
        $response['message'] = $message;

        $mailer->send($subject,$template);
        wp_send_json($response);
    }


}

add_action('wp_ajax_ayp_js_order', 'ayp_js_order');
add_action('wp_ajax_nopriv_ayp_js_order', 'ayp_js_order');
function ayp_js_order(){
    $errorArray = $message = $response = array();
    $error = false;
    $message['title'] = 'Спасибо';
    $message['subtitle'] = 'Ваша заявка отправлена';

    $name = purifyInput($_POST['name']);
    $phone = purifyInput($_POST['phone']);
    $email = purifyInput($_POST['email']);
    $services = purifyInput($_POST['services']);
    if (empty($name)){
        $nameErr = "Пожалуйста, укажите ваше имя";
        $error = true;
        $errorArray['name'] = array(
            'field' => 'name',
            'message' => $nameErr
        );
    }
    if (empty($phone)){
        $phoneErr = "Пожалуйста, укажите номер телефона";
        $error = true;
        $errorArray['phone'] = array(
            'field' => 'phone',
            'message' => $phoneErr
        );
    }
    else{
        if (!preg_match("/^[+0-9-()\s]*$/",$phone)) {
            $phoneErr = "Номер телефона должен содержать только цифры";
            $error = true;
            $errorArray['phone'] = array(
                'field' => 'phone',
                'message' => $phoneErr
            );
        }
    }
    if (empty($email)) {
        $emailErr = "Пожалуйста, укажите ваш email";
        $error = true;
        $errorArray['email'] = $emailErr;
    } else {
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Указан некорректный адрес почты";
            $error = true;
            $errorArray['email'] = array(
                    'field' => 'email',
                    'message' => $emailErr
            );
        }
    }
    if($error){
        $response['status'] = 'error';
        $response['error_list'] = $errorArray;
        wp_send_json($response);
    }

    else{
        global $ayp_redux;
        $mailer = new \Ayeps\Notification\Mailer($ayp_redux);
        $subject = "Новая заявка с сайта";
        $plain_text = "Написал(а):".$name."\nТелефон: ".$phone."\nEmail: ".$email."\nИнтересующие услуги: ".$services;
        saveApplication($plain_text,$subject);
        $rows = array(
            array(
                'name' => 'Новый заказ на запись',
                'value' => ' - '
            ),
            array(
                'name' => 'имя',
                'value' => $name
            ),
            array(
                'name' => 'Email',
                'value' => $email
            ),
            array(
                'name' => 'Телефон',
                'value' => $phone
            ),
            array(
                'name' => 'Интересующие услуги',
                'value' => $services
            )
        );
        $template = Timber::compile('email/admin/new-order.twig', array('rows'=>$rows));
        $response['status'] = 'success';
        $response['message'] = $message;

        $mailer->send($subject,$template);
        wp_send_json($response);
    }


}

function purifyInput($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
function saveApplication($plain_text, $subject, $type = 'ayp_order')
{
    $id = wp_insert_post(array(
        'post_content'=> $plain_text,
        'post_title' => $subject,
        'post_type' => $type,
        'post_status' => 'publish'
    ));

    return $id;
}

add_action('wp_ajax_ayp_js_save_napr', 'save_ayp_option');
function save_ayp_option(){


    $price_title = $_POST['price_title'];
    $price_time = $_POST['price_time'];
    $price_value = $_POST['price_value'];

    $data['data'] = array();
    for ($i=0;$i<count($price_title);$i++){
        $data['data'][] = array(
            'title' => $price_title[$i],
            'time' => $price_time[$i],
            'value' => $price_value[$i]
        );
    }

    update_option('ayp_lessons', serialize($data));

    $response['status'] = 'ok';

    wp_send_json($response);
}

class MySettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $lessons;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {

        // This page will be under "Settings"
        add_menu_page(
            'Наши направления обучения',
            'Направления обучения',
            'manage_options',
            'my-setting-admin',
            array( $this, 'create_admin_page' ),
            '',
            24
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->lessons = unserialize(get_option( 'ayp_lessons' ));
        ?>
        <style>
            .price_row{
                display: flex;
                margin-bottom:15px;
            }
            .price__col{
                flex:1;
                padding:0 15px;
            }
            .price__col input{
                width:100%;
            }
        </style>
        <div  class="wrap">
            <h1>Направления обучения</h1>
            <form  method="post" class="ayp_js_save_napr" action="options.php">
                <div class="price_row">
                    <div class="price__col" style="text-align: center;">Специальность</div>
                    <div class="price__col" style="text-align: center;">Продолжительность</div>
                    <div class="price__col" style="text-align: center;">Стоимость</div>
                </div>
                <div id="price-meta-container">
                    <?php if ($this->lessons['data']):
                        foreach ($this->lessons['data'] as $lesson){
                            ?>
                            <div class="price_row">
                                <div class="price__col"><input type="text" value="<?php echo $lesson['title']?>" name="price_title[]"></div>
                                <div class="price__col"><input type="text" value="<?php echo $lesson['time']?>" name="price_time[]"></div>
                                <div class="price__col"><input type="text" value="<?php echo $lesson['value']?>" name="price_value[]"></div>
                                <button class="js-remove-row">Удалить</button>
                            </div>
                            <?php
                        }
                    endif;
                    ?>
                    <div class="price_row">
                        <div class="price__col"><input type="text" name="price_title[]"></div>
                        <div class="price__col"><input type="text"  name="price_time[]"></div>
                        <div class="price__col"><input type="text"  name="price_value[]"></div>
                        <button class="js-remove-row">Удалить</button>
                    </div>
                </div>

                <input type="submit" style="float: right" value="Сохранить" class="button button-primary">
            </form>
            <button class="js-add-price-row button button-primary">Добавить направление обучения</button>
        </div>
        <script>
            jQuery(document).ready(function ($) {
                var act = 'ayp_js_save_napr';
                $('.ayp_js_save_napr').submit(function (e) {
                    e.preventDefault();
                    $.post('<?php echo admin_url( "admin-ajax.php" )?>', $(this).serialize() + '&action='+act, function (response) {
                        if(response.status === 'ok'){
                            alert("Информация сохранена");
                        }
                    });
                });
                $('.js-add-price-row').click(function (e) {
                    e.preventDefault();
                    $('#price-meta-container').append('<div class="price_row">' +
                        '<div class="price__col"><input type="text" name="price_title[]"></div>' +
                        '<div class="price__col"><input type="text"  name="price_time[]"></div>'+
                        '<div class="price__col"><input type="text"  name="price_value[]"></div>' +
                        '<button class="js-remove-row">Удалить</button>' +
                        '</div>');
                });
                $('#price-meta-container').on('click', '.js-remove-row', function (e) {
                    e.preventDefault();
                    $(this).parents('.price_row').remove();
                });
            });
        </script>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'my_option_group', // Option group
            'my_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'My Custom Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );

        add_settings_field(
            'id_number', // ID
            'ID Number', // Title
            array( $this, 'id_number_callback' ), // Callback
            'my-setting-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'title',
            'Title',
            array( $this, 'title_callback' ),
            'my-setting-admin',
            'setting_section_id'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['id_number'] ) )
            $new_input['id_number'] = absint( $input['id_number'] );

        if( isset( $input['title'] ) )
            $new_input['title'] = sanitize_text_field( $input['title'] );

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function id_number_callback()
    {
        printf(
            '<input type="text" id="id_number" name="my_option_name[id_number]" value="%s" />',
            isset( $this->options['id_number'] ) ? esc_attr( $this->options['id_number']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function title_callback()
    {
        printf(
            '<input type="text" id="title" name="my_option_name[title]" value="%s" />',
            isset( $this->options['title'] ) ? esc_attr( $this->options['title']) : ''
        );
    }
}

if( is_admin() )
    $my_settings_page = new MySettingsPage();