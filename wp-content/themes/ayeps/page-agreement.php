<?php
/**
 * Template Name: Политика обработки данных
 *
 */
?>

<html lang="ru"><head>
    <meta charset="UTF-8">
    <title>Пользовательское соглашение</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Roboto:700,400,300,200,100&subset=latin,cyrillic);
        body{
            font-family: "Roboto", sans-serif;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            line-height: 1.42857143;
            color: #677273;
            padding-top: 20px;}
        .box{
            width: 940px;
            margin: 0 auto;
        }
        .center{text-align: center;}
        h1{
            font-size:2em;
            text-align: center;
        }
        ol li {
        }
    </style>
</head>
<body>
<div class="box">
    <h1><?php the_title();?></h1>
    <?php if ( have_posts() ) : ?>

        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();

            /*
             * Include the Post-Format-specific template for the content.
             * If you want to override this in a child theme, then include a file
             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
             */
            the_content( );

            // End the loop.
        endwhile;

    endif;
    ?>
</div>

</body></html>