module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        "babel": {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    "assets/dist/es6/main.js": "assets/src/es6/main.js"
                }
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')
                ]
            },
            dist: {
                src: 'assets/src/css/*.css'
            }
        },
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: [{
                    // Set to true for recursive search
                    expand: true,
                    cwd: 'assets/src/scss/',
                    src: ['**/*.scss'],
                    dest: 'assets/src/css/',
                    ext: '.css'
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/src/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'assets/dist/css',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                mangle: {
                    reserved: ['jQuery', 'Backbone']
                }
            },
            my_target: {
                files:[{
                    expand: true,
                    cwd: 'assets/src/js',
                    src: ['**/*.js','!*.min.js'],
                    dest: 'assets/dist/js',
                    ext: '.min.js'
                }]
            }
        },
        smushit: {
            mygroup: {
                src: ['assets/images/**/*.png','assets/images/**/*.jpg'],
                dest: 'assets/dist/img'
            }
        },
        purifycss: {
            options: {},
            target: {
                src: ['*.html'],
                css: ['assets/dist/css/mui.min.css'],
                dest: 'assets/tmp/mui-pure.css'
            },
        },
        cachebreaker: {
            dev: {
                options: {
                    match: ['common.js', 'style.css'],
                },
                files: {
                    src: ['index.html', '*/index.html', 'templates/base.twig']
                }
            }
        },
        svgstore: {
            options: {
                includeTitleElement: false,
                svg: {
                    style: 'display:none',
                },
                cleanup: true,
                cleanupdefs :true
            },
            default : {
                files: {
                    'assets/dist/images/sprite.svg': ['assets/images/svg/*.svg'],
                },
            },
        },
    });

    grunt.registerTask('default', ['sass:dist','postcss:dist','cssmin', 'uglify', 'cachebreaker']);
};